# usb_meter_um25

Attempt to build a python connector for the Ruideng UM25 USB power meter.

Notes:
* Device connects via Bluetooth (default device name of UM25C) and presents a serial device.
---
* Data we want:
  * Voltage
  * Amperage
  * Power (Watts)
  * Resistance